/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {
  /***************************************************************************
   * Set the default database connection for models in the production        *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  // models: {
  //   connection: 'someMysqlServer'
  // },

  /***************************************************************************
   * Set the port in the production environment to 80                        *
   ***************************************************************************/

  // port: 80,

  /***************************************************************************
   * Set the log level in production environment to "silent"                 *
   ***************************************************************************/

  connections: {
    ops: {
      adapter: 'sails-mongo',
      host: process.env.OPENSHIFT_MONGODB_DB_HOST || '172.30.38.89',
      port: process.env.OPENSHIFT_MONGODB_DB_PORT || 27017,
      user: 'admin',
      password: process.env.MONGODB_ADMIN_PASSWORD || 'KqrfbAauSBGq',
      database: 'ntalk'
    }
  },
  models: {
    schema: true,
    connection: 'ops',
    migrate: 'alter'
  },
  log: {
    level: "verbose"
  },
  port: process.env.OPENSHIFT_NODEJS_PORT || 8080,
  host: process.env.OPENSHIFT_NODEJS_IP || "172.30.20.57",
};
