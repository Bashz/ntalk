/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var languages = require('../utils/languages').map(function (lang) {
  return lang.fb;
});
var genders = ["male", "female", "any"];
module.exports = {
  attributes: {
    fbId: {
      type: "string",
      required: true,
      unique: true,
      index: true
    },
    first_name: {
      type: "string",
      required: true
    },
    last_name: {
      type: "string",
      required: true
    },
    profile_pic: {
      type: "string",
      required: true
    },
    locale: {
      type: "string",
      enum: languages
    },
    timezone: {
      type: "integer"
    },
    lat: {
      type: "float",
      defaultsTo: 999
    },
    long: {
      type: "float",
      defaultsTo: 999
    },
    gender: {
      type: "string"
    },
    peer: {
      model: 'user'
    },
    creating_hub: {
      type: 'boolean',
      defaultsTo: false,
      required: true
    },
    connected: {
      type: 'boolean',
      defaultsTo: true,
      required: true
    },
    room: {
      model: 'hub'
    },
    country: {
      type: 'string'
    },
    tags: {
      type: 'array',
      defaultsTo: []
    },
    favorits: {
      type: 'array',
      defaultsTo: []
    },
    recents: {
      type: 'array',
      defaultsTo: []
    },
    quick: {
      type: 'boolean'
    },
    intersted_by: {
      type: "string",
      enum: genders,
      defaultsTo: "any"
    },
    unbind: function (error, success, next) {
      var that = this;
      if (this.peer) {
        var peer = JSON.parse(JSON.stringify(this.peer));
        if (this.peer.recents.indexOf(this.id) === -1)
          this.peer.recents.unshift(this.id);
        if (this.recents.indexOf(this.peer.id) === -1)
          this.recents.unshift(this.peer.id);
        if (this.recents.length > 10)
          this.recents.splice(10);
        if (this.peer.recents.length > 10)
          this.peer.recents.splice(10);
        this.peer.peer = null;
        this.save(function (err) {
          if (err)
            return error(that, err);
          that.peer = null;
          that.save(function (err) {
            if (err)
              return error(that, err);
            success(peer, null, that);
            next();
          });
        });
      } else if (this.room) {
        var hub = JSON.parse(JSON.stringify(this.room));
        this.room.guests--;
        this.save(function (err) {
          if (err)
            return error(that, err);
          that.room = null;
          that.save(function (err) {
            if (err)
              return error(that, err);
            success(null, hub, that);
            next();
          });
        });
      } else {
        success(null, null, that);
        next();
      }
    },
    disconnect: function (error, success) {
      var that = this;
      this.connected = false;
      this.quick = false;
      this.save(function (err) {
        if (err)
          return error(that, err);
        success();
      });
    },
    connect: function (error, success) {
      var that = this;
      this.connected = true;
      this.save(function (err) {
        if (err)
          return error(that, err);
        success();
      });
    },
    creatingHub: function (error, success) {
      var that = this;
      Hub.create({owner: this.id}).exec(function (err, hub) {
        if (err)
          return error(that, err);
        that.creating_hub = true;
        that.save(function (err) {
          if (err)
            return error(that, err);
          success();
        });
      });
    },
    leaveHub: function (error, success) {
      var that = this;
      var hub = this.room ? JSON.parse(JSON.stringify(this.room)) : null;
      success(hub);
//      this.room = null;
//      this.save(function (err) {
//        if (err)
//          return error(that, err);
//        success(hub);
//      });
    },
    joinHub: function (hubId, error, success) {
      var that = this;
      Hub.findOne({id: hubId}).exec(function (err, hub) {
        if (err)
          return error(that, err);
        that.room = hub.id;
        hub.guests++;
        hub.save(function (err) {
          if (err)
            return error(that, err);
          that.save(function (err) {
            if (err)
              return error(that, err);
            success(hub);
          });
        });
      });
    },
    setLang: function (language, error, success) {
      var that = this;
      this.locale = language.fb;
      this.save(function (err) {
        if (err)
          return error(that, err);
        success();
      });
    },
    setLatLong: function (coordinates, error, success) {
      this.lat = coordinates.lat;
      this.long = coordinates.long;
      this.save(function (err) {
        if (err)
          return error(that, err);
        success();
      });
    }
  },
  createFromFb: function (id, cb) {
    https = require('https');
    var options = {
      hostname: 'graph.facebook.com',
      port: 443,
      path: '/' + sails.config.parameters.fbApiVersion + '/' + id + '?access_token=' + sails.config.parameters.pageAccessToken,
      method: 'GET'
    };

    var req = https.request(options, function (res) {
      var body = '';
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
        body += chunk;
      });
      res.on('end', function () {
        var fbUser = JSON.parse(body);
        fbUser.fbId = id;
        User.create(fbUser)
          .exec(function (err, user) {
            if (err)
              return cb(err, null);
            cb(null, user);
          });
      });
    });
    req.on('error', function (err) {
      return cb(err, null);
    });
    req.end();
  },
  findByHub: function (user, hub, error, success) {
    User.find({room: hub.id, id: {$nin: [user.id, user.peer]}}).exec(function (err, users) {
      if (err)
        return error(user, err);
      success(users);
    });
  },
  getOthers: function (user, page, error, success) {
    User.find({connected: true, peer: null, id: {$nin: [user.id, user.peer]}}).sort('updatedAt DESC').limit(4).skip(page * 4)
      .exec(function (err, users) {
        if (err)
          return error(user, err);
        success(users);
      });
  },
  inUserHub: function (user, error, success) {
    if (user.room)
      User.find({room: user.room.id}).sort('updatedAt DESC').exec(function (err, users) {
        if (err)
          return error(user, err);
        success(users);
      });
    else
      success(null)
  },
  queue: function (user, error, success) {
    User.findOne({connected: true, quick: true, id: {$nin: [user.id, user.peer]}})
      .populate('peer')
      .populate('room')
      .sort('updatedAt DESC')
      .exec(function (err, waitingUser) {
        if (err)
          return error(user, err);
        if (!waitingUser) {
          user.quick = true;
          user.save(function (err) {
            if (err)
              return error(user, err);
          });
          success([]);
        } else
          success([user, waitingUser]);
      });
  },
  bind: function (users, error, success) {
    users[1].peer = users[0].id;
    users[1].quick = false;
    users[0].peer = users[1].id;
    users[0].quick = false;
    users[0].save(function (err) {
      if (err)
        return error(users[0], err);
      users[1].save(function (err) {
        if (err)
          return error(users[1], err);
        success(users);
      });
    });
  },
  favorites: function (user, error, success) {
    User.find({id: {$in: user.favorits}}).sort('updatedAt DESC').exec(function (err, users) {
      if (err)
        return error(user, err);
      success(users);
    });
  },
  recents: function (user, error, success) {
    User.find({id: {$in: user.recents}}).sort('updatedAt DESC').exec(function (err, users) {
      if (err)
        return error(user, err);
      success(users);
    });
  },
  addFavorit: function (user, favId, error, success) {
    User.findOne({id: favId}).exec(function (err, fav) {
      if (err)
        return error(user, err);
      if (user.favorits.indexOf(favId) === -1)
        user.favorits.unshift(favId);
      if (user.favorits.length > 10)
        user.favorits.splice(10);
      user.save(function (err) {
        if (err)
          return error(user, err);
        success(fav);
      });
    });
  },
  getByFb: function (user, userFbId, error, success) {
    User.findOne({fbId: userFbId}).exec(function (err, requested) {
      if (err)
        return error(user, err);
      success(requested);
    });
  },
  getByFacebook: function (user, userFbId, error, success) {
    User.findOne({fbId: userFbId}).populate('peer').populate('room').exec(function (err, requested) {
      if (err)
        return error(user, err);
      success(requested);
    });
  },
  search: function (user, hint, error, success) {
    var rgx = new RegExp(hint, 'i');
    User.find({
      id: {
        $nin: [user.id, user.peer]
      },
      $or: [
        {tags: hint},
        {first_name: rgx},
        {last_name: rgx},
        {country: rgx}
      ]
    }).sort('updatedAt DESC').limit(8).exec(function (err, users) {
      if (err)
        return error(user, err);
      Hub.find({name: rgx}).limit(2).exec(function (err, hubs) {
        if (err)
          return error(user, err);
        success(users, hubs);
      });
    });
  },
  nearBy: function (user, error, success) {
    User.find({
      connected: true,
      id: {
        $nin: [user.id, user.peer]
      },
      lat: {$gt: (user.lat - 1), $lt: (user.lat + 1)},
      long: {$gt: (user.long - 1), $lt: (user.long + 1)}
    }).sort('updatedAt DESC').limit(10).exec(function (err, users) {
      if (err)
        return error(user, err);
      success(users);
    });
  }
};

