/**
 * Hub.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    name: {
      type: "string",
      unique: true,
      index: true
    },
    image: {
      type: "string",
    },
    description: {
      type: "string"
    },
    welcome_msg: {
      type: "string"
    },
    owner: {
      model: 'user',
      required: false
    },
    users: {
      collection: 'user',
      via: 'room'
    },
    guests: {
      type: "integer",
      defaultsTo: 0
    },
    tags: {
      type: 'array'
    }
  },
  all: function (user, page, error, success) {
    Hub.find({name: {$exists: true}, description: {$exists: true}})
        .populate('users')
        .sort('guests DESC')
        .limit(4)
        .skip(page * 4)
        .exec(function (err, hubs) {
          if (err)
            return error(user, err);
          success(hubs);
        });
  },
  creation: function (user, message, error, success) {
    Hub.find({owner: user.id}).sort('updatedAt DESC').exec(function (err, hubs) {
      if (err)
        return error(user, err);
      if (!hubs.length)
        return error(user, "No hub found during creation!");
      var hub = hubs[0];
      var next = "";
      if (!message)
        return success("The input was malformed :( \nPlease try again");
      if (message.text) {
        if (message.text.match(/^done/i)) {
          next = "well done! you can join your hub by typing #" + hub.name;
          user.creating_hub = false;
          user.save(function (err) {
            if (err)
              return error(user, err);
          });
        } else {
          if (!hub.name) {
            hub.name = message.text;
            next = "A short description of your hub would help :)";
          } else if (!hub.description) {
            hub.description = message.text;
            next = "Let's add a welcome message !\nOr type 'done' to finish the creation 🚀";
          } else if (!hub.welcome_msg) {
            hub.welcome_msg = message.text;
            next = "Almost there, please send a picture attachement for your hub 8)\nOr type 'done' to finish the creation 🚀";
          }
        }
      } else {
        if (message.attachments[0].type === "image") {
          if (hub.name) {
            if (hub.description) {
              if (hub.welcome_msg) {
                hub.image = message.attachments[0].payload.url;
                next = "well done!";
                user.creating_hub = false;
                user.save(function (err) {
                  if (err)
                    return error(user, err);
                });
              } else
                return success("Not yet, First let's add a welcome message !.");
            } else
              return success("Not yet, First add a short description to your hub");
          } else
            return success("Not yet, First give your new hub a name");
        } else {
          return success("This file format is not supported\nPlease send an image file.");
        }
      }
      hub.save(function (err) {
        if (err)
          return error(user, err);
        success(next);
      })
    })
  }
};

