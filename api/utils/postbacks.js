/**
 * PostbackController
 *
 * @description :: Server-side logic for managing postbacks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var sendAPI = require('../utils/sendAPI');

module.exports = function (postback, user, fallback) {
  if (postback.payload) {
    var payload = postback.payload;
    /*
     * 
     */
    if (payload === "MENU_DISCONNECT") {
      if (user.peer) {
        User.unbind(user, function (err, users) {
          if (err)
            return sendAPI.reportError(user, err, fallback);
          sendAPI.peerDisconnected(users[1], users[0], fallback);
        });
      }
      user.disconnect(function (err) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.text(user, "You are now disconnected\nYou can not be requested to chat and will not receive messages", fallback);
      });
    }
    /*
     * 
     */
    else if (payload === "MENU_CONNECT") {
      user.connect(function (err) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.text(user, "You are now connected\nPeople can send you chat requests :)", fallback);
      });
    }
    /*
     * 
     */
    else if (payload.match(/^MORE_FRIEND/)) {
      var page = parseInt(payload.split(':')[1]);
      User.getOthers(user, page, function (err, users) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.contactTemplate(user, users, page, "MORE_FRIEND", fallback);
      });
    }
    /*
     * 
     */
    else if (payload === "QUICK_CONNECT") {
      User.queue(user, function (err, users) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        if (users.length) {
          User.bind(users, function (err, users) {
            if (err)
              return sendAPI.reportError(user, err, fallback);
            sendAPI.bound(users[0], users[1], fallback);
          });
        } else {
          sendAPI.text(user, "You are now in a queue we will match you soon with someone!", fallback);
        }
      });
    }
    /*
     * 
     */
    else if (payload.match(/^SHOW_HUB_USERS/)) {
      var page = parseInt(payload.split(':')[1]);
      User.inUserHub(user, function (err, users) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.contactTemplate(user, users, page, "SHOW_HUB_USERS", fallback);
      });
    }
    /*
     * 
     */
    else if (payload.match(/^SHOW_HUBS/)) {
      var page = parseInt(payload.split(':')[1]);
      Hub.all(function (err, hubs) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.hubsTemplate(user, hubs, page, "SHOW_HUBS", fallback);
      });
    }
    /*
     * 
     */
    else if (payload === "CREATE_HUB") {
      user.creatingHub(function (err) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.text(user, "Give your hub a name :)", fallback);
      });
    }
    /*
     * 
     */
    else if (payload === "DISCONNECT_HUB") {
      user.leaveHub(function (err, hub) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        if (hub)
          sendAPI.text(user, "Successfuly left " + hub.name, fallback);
        else
          sendAPI.text(user, "You do not belong to a hub :p", fallback);
      })
    }
    /*
     * 
     */
    else if (payload.match(/CONNECT_HUB/)) {
      var hubId = payload.split(':')[1];
      user.entreHub(hubId, function (err, hub) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        if (hub)
          sendAPI.text(user, hub.welcome, fallback);
        else
          return sendAPI.text(user, "You are already in the requested hub :p", fallback);
        if (user.peer) {
          User.unbind(user, function (err, users) {
            if (err)
              return sendAPI.reportError(user, err, fallback);
            sendAPI.peerDisconnected(users[1], users[0], fallback);
          });
        }
      });
    }
    /*
     * 
     */
    else if (payload.match(/^RECENT/)) {
      User.recents(user, function (err, users) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.contactTemplate(user, users, page, "RECENT", fallback);
      });
    }
    /*
     * 
     */
    else if (payload.match(/^FAVORIT/)) {
      User.favorit(user, function (err, users) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.contactTemplate(user, users, page, "FAVORIT", fallback);
      });
    }
    /*
     * 
     */
    else if (payload.match(/^ADD_FAVORIT/)) {
      userId = payload.split(':')[1];
      user.addFavorit(user, userId, function (err, favorit) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.text(user, favorit.name + " successfuly added to your favorits !!", fallback);
      });
    }
    /*
     * 
     */
    else if (payload === "SEARCH") {
      sendAPI.text(user, "Type # followed by your keywrods to search among users", fallback);
    }
    /*
     * 
     */
    else if (payload.match(/MENU_LANG/)) {
      var lang = payload.split(':')[1];
      getLang(lang, function (err, language) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        if (!language)
          return sendAPI.text(user, "Unreconized language :(", fallback);
        user.setLang(language.fb, function (err) {
          if (err)
            return sendAPI.reportError(user, err, fallback);
          sendAPI.text(user, "Language changed to " + language.name, fallback);
        });
      });
    }
    /*
     * 
     */
    else if (payload.match(/^REQUEST_TO/)) {
      var userFbId = payload.split(':')[1];
      User.getByFb(userFbId, function (err, requested) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        sendAPI.requestingTalk(user, requested, fallback);
        sendAPI.requestedTalk(requested, user, fallback);
      })
    }
    /*
     * 
     */
    else if (payload.match(/^CONNECT_TO/)) {
      var userFbId = payload.split(':')[1];
      User.getByFb(userFbId, function (err, requested) {
        if (err)
          return sendAPI.reportError(user, err, fallback);
        User.bind([user, requested], function (err, users) {
          if (err)
            return sendAPI.reportError(user, err, fallback);
          sendAPI.bound(users[0], users[1], fallback);
        });
      });
    } else {
      unreconizedPostback(postback);
    }
  } else {
    unreconizedPostback(postback);
  }
}

unreconizedPostback = function (postback) {
  sails.log.warn("Recived an unreconized postback, as bellow :");
  sails.log.info(postback);
}