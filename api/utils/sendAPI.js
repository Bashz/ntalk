var https = require('https');
var languages = require('../utils/languages');

module.exports = {
  send: function (messageData, cb) {
    messageData.access_token = sails.config.parameters.pageAccessToken;
    var data = JSON.stringify(messageData);
    var options = {
      hostname: 'graph.facebook.com',
      port: 443,
      path: '/' + sails.config.parameters.fbApiVersion + '/me/messages',
      qs: {access_token: sails.config.parameters.pageAccessToken},
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Content-Length': Buffer.byteLength(data)
      }
    };
    var req = https.request(options, function (res) {
      var body = '';
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
        body += chunk;
      });
      res.on('end', function () {
        var message = JSON.parse(body);
        if (!message)
          return cb('error while parsing json response, response was : ' + body, null);
        if (message.error)
          return cb(message, null);
        return cb(null, message);
      });
    });
    req.on('error', function (err) {
      return sails.log.error(err);
    });
    req.write(data);
    req.end();
  },
  typingOn: function (recipientId, done) {
    var messageData = {
      recipient: {
        id: recipientId
      },
      sender_action: "typing_on"
    };
    this.send(messageData, done);
  },
  typingOff: function (recipientId, done) {
    var messageData = {
      recipient: {
        id: recipientId
      },
      sender_action: "typing_off"
    };
    this.send(messageData, done);
  },
  welcome: function (user, done) {
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        text: "Hello, and welcome to nTalk :)\nnTalk allow you to chat with friends or stranger without worrying about language barriers!!\nYes! 8) it translate everything for you.\n\nDo not forget to check the menu 👇 for additional options",
        quick_replies: [
          {
            content_type: "text",
            title: "🌍 Nearby",
            payload: "NEARBY"
          },
          {
            content_type: "text",
            title: "🚀 Quick chat",
            payload: "QUICK_CONNECT"
          },
          {
            content_type: "text",
            title: "👫 Find friends",
            payload: "MORE_FRIEND:0"
          },
          {
            content_type: "text",
            title: "🏨 Join a hub",
            payload: "SHOW_HUBS:0"
          }
        ]
      }
    };
    this.send(messageData, done);
  },
  requestingTalk: function (user, requested, done) {
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        text: "📧 The Invitation was sent to " + requested.first_name + " " + requested.last_name
      }
    };
    this.send(messageData, done);
  },
  requestedTalk: function (requested, user, done) {
    var messageData = {
      recipient: {
        id: requested.fbId
      },
      message: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [{
                title: 'Chat request from ' + user.first_name + ' ' + user.last_name,
                image_url: user.profile_pic,
                subtitle: "Language: " + getLan(user.locale) + "\n" +
                    "Timezone: " + (user.timezone >= 0 ? "+" + user.timezone : "-" + user.timezone) + "\n" +
                    "Gender: " + user.gender + "\n",
                buttons: [{
                    type: "postback",
                    title: "Accept",
                    payload: "CONNECT_TO:" + user.fbId
                  }]
              }
            ]
          }
        }
      }
    };
    this.send(messageData, done);
  },
  connected: function (user, done) {
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        text: "You are now connected\nPeople can send you chat requests :)"
      }
    };
    this.send(messageData, done);
  },
  translate: function (user, peer, tr, done) {
    var face = "👦";
    if (user.gender === "female")
      face = "👧";
    var messageData = {
      recipient: {
        id: peer.fbId
      },
      message: {
        text: face + " " + user.first_name + ":\n" + tr.text[0]
      }
    };
    this.send(messageData, done);
  },
  read: function (user, done) {
    var messageData = {
      recipient: {
        id: user.peer.fbId
      },
      sender_action: "mark_seen"
    };
    this.send(messageData, done);
  },
  sendAttachement: function (user, peer, attachment, done) {
    var messageData = {
      recipient: {
        id: peer.fbId
      },
      message: {
        attachment: attachment
      }
    };
    this.send(messageData, done);
  },
  text: function (user, text, done) {
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        text: text
      }
    };
    this.send(messageData, done);
  },
  contactTemplate: function (user, users, page, topic, done) {
    if (users.length < 2) {
      var messageData = {
        recipient: {
          id: user.fbId
        },
        message: {
          attachment: {
            type: "template",
            payload: {
              template_type: "generic",
              elements: [
                {
                  title: "No online users were found",
                  subtitle: "Please help us to reach more people to connect to!",
                  buttons: [
                    {
                      type: "element_share",
                      share_contents: {
                        attachment: {
                          type: "template",
                          payload: {
                            template_type: "generic",
                            elements: [
                              {
                                title: "Want to chat with people without language barriers ?",
                                subtitle: "Join nTalk and talk with users around the world without worrying about language :)",
                                image_url: "https://scontent.ftun2-1.fna.fbcdn.net/v/t1.0-1/p200x200/18119360_252290531911376_7339226112817539071_n.png?oh=e19008ca4a3473d31c1fdb73876c04d4&oe=59950C35",
                                default_action: {
                                  type: "web_url",
                                  url: "https://m.me/249861258820970"
                                },
                                buttons: [
                                  {
                                    type: "web_url",
                                    url: "https://m.me/249861258820970?ref=nTalk",
                                    title: "Join me",
                                  }
                                ]
                              }
                            ]
                          }
                        }
                      }
                    }
                  ]
                }
              ]
            }
          }
        }
      };
    } else {
      var elements = [];
      for (var i = 0; i < users.length; i++) {
        var U = users[i];
        elements.push({
          title: U.first_name + ' ' + U.last_name,
          image_url: U.profile_pic,
          subtitle: "Language " + getLan(U.locale) + "\n" +
              "Timezone " + (U.timezone >= 0 ? "+" + U.timezone : "-" + U.timezone) + "\n" +
              "Gender " + U.gender + "\n",
          buttons: [{
              type: "postback",
              title: "Invite to chat",
              payload: "REQUEST_TO:" + U.fbId
            }]
        });
      }
      var messageData = {
        recipient: {
          id: user.fbId
        },
        message: {
          attachment: {
            type: "template",
            payload: {
              template_type: "list",
              top_element_style: "compact",
              elements: elements,
              buttons: [
                {
                  title: "Find More",
                  type: "postback",
                  payload: topic + ":" + (page + 1)
                }
              ]
            }
          }
        }
      };
    }
    this.send(messageData, done);
  },
  hubsTemplate: function (user, hubs, page, topic, done) {
    if (hubs.length < 2) {
      var messageData = {
        recipient: {
          id: user.fbId
        },
        message: {
          attachment: {
            type: "template",
            payload: {
              template_type: "generic",
              elements: [
                {
                  title: "No more hubs where found :(",
                  subtitle: "Do not worry you can create your own hub!",
                  buttons: [
                    {
                      type: "postback",
                      title: "Create a new Hub",
                      payload: "CREATE_HUB"
                    }
                  ]
                }
              ]
            }
          }
        }
      };
    } else {
      var elements = [];
      for (var i = 0; i < hubs.length; i++) {
        var H = hubs[i];
        elements.push({
          title: H.name + " (" + H.users.length + " users)",
          image_url: H.image,
          subtitle: H.description,
          buttons: [{
              type: "postback",
              title: "Join " + H.name,
              payload: "CONNECT_HUB:" + H.id
            }]
        });
      }
      var messageData = {
        recipient: {
          id: user.fbId
        },
        message: {
          attachment: {
            type: "template",
            payload: {
              template_type: "list",
              top_element_style: "compact",
              elements: elements,
              buttons: [
                {
                  title: "Show me more hubs!",
                  type: "postback",
                  payload: topic + ":" + (page + 1)
                }
              ]
            }
          }
        }
      };
    }
    this.send(messageData, done);
  },
  hubDisconnected: function (inhub, user, done) {
    var messageData = {
      recipient: {
        id: inhub.fbId
      },
      message: {
        text: "⬅" + user.first_name + " " + user.last_name + "🚶 has left the hub."
      }
    };
    this.send(messageData, done);
  },
  hubConnected: function (inhub, user, done) {
    var messageData = {
      recipient: {
        id: inhub.fbId
      },
      message: {
        text: "➡" + user.first_name + " " + user.last_name + "🏃 has joined this hub."
      }
    };
    this.send(messageData, done);
  },
  peerDisconnected: function (peer, user, done) {
    var messageData = {
      recipient: {
        id: peer.fbId
      },
      message: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [{
                title: "Discussion with " + user.first_name + " " + user.last_name + " has ended!",
                image_url: user.profile_pic,
                subtitle: "Language: " + getLan(user.locale) + "\n" +
                    "Timezone: " + (user.timezone >= 0 ? "+" + user.timezone : "-" + user.timezone) + "\n" +
                    "Gender: " + user.gender + "\n",
                buttons: [{
                    type: "postback",
                    title: "Add to favorits",
                    payload: "ADD_FAVORIT:" + user.id
                  }]
              }
            ]
          }
        }
      }
    };
    this.send(messageData, done);
  },
  reportError: function (user, err, done) {
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        text: "Ooops, Something went wrong :( , this accident was reported!\nplease try again later."
      }
    };
    this.send(messageData, done);
    var messageDataAdmin = {
      recipient: {
        id: sails.config.parameters.reportTo
      },
      message: {
        text: JSON.stringify(err)
      }
    };
    this.send(messageDataAdmin, done);
  },
  bound: function (users, done) {
    var messageData = {
      recipient: {
        id: users[0].fbId
      },
      message: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [{
                title: "You are now connected with " + users[1].first_name + " " + users[1].last_name + " enjoy talking :)",
                image_url: users[1].profile_pic,
                subtitle: "Language: " + getLan(users[1].locale) + "\n" +
                    "Timezone: " + (users[1].timezone >= 0 ? "+" + users[1].timezone : "-" + users[1].timezone) + "\n" +
                    "Gender: " + users[1].gender + "\n"
              }
            ]
          }
        }
      }
    };
    this.send(messageData, done);
    var messageData = {
      recipient: {
        id: users[1].fbId
      },
      message: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [{
                title: "You are now connected with " + users[0].first_name + " " + users[0].last_name + " enjoy talking :)",
                image_url: users[0].profile_pic,
                subtitle: "Language: " + getLan(users[0].locale) + "\n" +
                    "Timezone: " + (users[0].timezone >= 0 ? "+" + users[0].timezone : "-" + users[0].timezone) + "\n" +
                    "Gender: " + users[0].gender + "\n"
              }
            ]
          }
        }
      }
    };
    this.send(messageData, done);
  },
  askLocation: function (user, done) {
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        text: "🌍 Please share your location:",
        quick_replies: [
          {
            content_type: "location",
          }
        ]
      }
    };
    this.send(messageData, done);
  },
  contactItems: function (user, users, emptyText, done) {
    if (!users.length) {
      var messageData = {
        recipient: {
          id: user.fbId
        },
        message: {
          text: emptyText
        }
      };
      return this.send(messageData, done);
    }
    var elements = [];
    for (var i = 0; i < users.length; i++) {
      var U = users[i];
      elements.push({
        title: U.first_name + ' ' + U.last_name,
        image_url: U.profile_pic,
        subtitle: "Language " + getLan(U.locale) + "\n" +
            "Timezone " + (U.timezone >= 0 ? "+" + U.timezone : "-" + U.timezone) + "\n" +
            "Gender " + U.gender + "\n",
        buttons: [{
            type: "web_url",
            url: "https://ntalk-bashz.rhcloud.com/profile/" + U.fbId,
            title: "Profile",
            webview_height_ratio: "compact",
            webview_share_button: "hide"
          }, {
            type: "postback",
            title: "Invite to chat",
            payload: "REQUEST_TO:" + U.fbId
          }]
      });
    }
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: elements
          }
        }
      }
    };
    this.send(messageData, done);
  },
  mixedItems: function (user, users, hubs, emptyText, done) {
    if (!users.length && !hubs.length) {
      var messageData = {
        recipient: {
          id: user.fbId
        },
        message: {
          text: emptyText
        }
      };
      return this.send(messageData, done);
    }
    var elements = [];
    for (var i = 0; i < hubs.length; i++) {
      var H = hubs[i];
      elements.push({
        title: H.name,
        image_url: H.image,
        subtitle: H.description,
        buttons: [{
            type: "postback",
            title: "Join hub",
            payload: "CONNECT_HUB:" + H.id
          }]
      });
    }
    for (var i = 0; i < users.length; i++) {
      var U = users[i];
      elements.push({
        title: U.first_name + ' ' + U.last_name,
        image_url: U.profile_pic,
        subtitle: "Language " + getLan(U.locale) + "\n" +
            "Timezone " + (U.timezone >= 0 ? "+" + U.timezone : "-" + U.timezone) + "\n" +
            "Gender " + U.gender + "\n",
        buttons: [{
            type: "web_url",
            url: "https://ntalk-bashz.rhcloud.com/profile/" + U.fbId,
            title: "Profile",
            webview_height_ratio: "compact",
            webview_share_button: "hide"
          }, {
            type: "postback",
            title: "Invite to chat",
            payload: "REQUEST_TO:" + U.fbId
          }]
      });
    }
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: elements
          }
        }
      }
    };
    this.send(messageData, done);
  },
  qrContacts: function (user, done) {
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        text: "Choose among favorites or people you recently talked to",
        quick_replies: [
          {
            content_type: "text",
            title: "🕒 Historic",
            payload: "RECENT"
          },
          {
            content_type: "text",
            title: "💞 Favorites",
            payload: "FAVORIT"
          }
        ]
      }
    };
    this.send(messageData, done);
  },
  noPeer: function (user, done) {
    var quickReplies = [
      {
        content_type: "text",
        title: "🌍 Nearby",
        payload: "NEARBY"
      },
      {
        content_type: "text",
        title: "🚀 Quick chat",
        payload: "QUICK_CONNECT"
      },
      {
        content_type: "text",
        title: "👫 Find friends",
        payload: "MORE_FRIEND:0"
      },
      {
        content_type: "text",
        title: "🏨 Join a hub",
        payload: "SHOW_HUBS:0"
      }
    ];
    if (user.recents.length)
      quickReplies.unshift({
        content_type: "text",
        title: "🕒 Historic",
        payload: "RECENT"
      });
    if (user.favorits.length)
      quickReplies.unshift({
        content_type: "text",
        title: "💞 Favorites",
        payload: "FAVORIT"
      });
    var messageData = {
      recipient: {
        id: user.fbId
      },
      message: {
        text: "It seems that you are not connected with someone :/ \nPlease do so by choosing from the 👇 options below 👇",
        quick_replies: quickReplies
      }
    };
    this.send(messageData, done);
  },
  myProfile: function (user, done) {
    var messageData = {
      "recipient": {
        id: user.fbId
      },
      "message": {
        "attachment": {
          "type": "template",
          "payload": {
            "template_type": "button",
            "text": "Check or edit your profile?",
            "buttons": [
              {
                "type": "web_url",
                "url": "https://ntalk-bashz.rhcloud.com/profile/" + user.fbId,
                "title": "Check",
                "webview_height_ratio": "compact",
                webview_share_button: "hide"
              },
              {
                "type": "web_url",
                "url": "https://ntalk-bashz.rhcloud.com/my/profile/",
                "title": "Edit",
                "webview_height_ratio": "full",
                "messenger_extensions": true,
                "fallback_url": "https://ntalk-bashz.rhcloud.com/my/profile/" + user.fbId,
                webview_share_button: "hide"
              }
            ]
          }
        }
      }
    };
    this.send(messageData, done);
  }
}
getLan = function (fblang) {
  var fb = languages.map(function (lang) {
    return lang.fb;
  });
  var ind = fb.indexOf(fblang);
  if (ind !== -1) {
    return languages[ind].name;
  } else {
    return "English";
  }
}