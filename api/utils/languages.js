module.exports = [
  {fb: "af_ZA", fallback: "af", yandex: "af", name: "Afrikaans", supported: true},
  {fb: "ak_GH", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "am_ET", fallback: "am", yandex: "am", name: "Amharic", supported: true},
  {fb: "ar_AR", fallback: "ar", yandex: "ar", name: "Arabic", supported: true},
  {fb: "as_IN", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ay_BO", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "az_AZ", fallback: "az", yandex: "az", name: "Azerbaijan", supported: true},
  {fb: "be_BY", fallback: "be", yandex: "be", name: "Belarusian", supported: true},
  {fb: "bg_BG", fallback: "bg", yandex: "bg", name: "Bulgarian", supported: true},
  {fb: "bn_IN", fallback: "bn", yandex: "bn", name: "Bengali", supported: true},
  {fb: "bp_IN", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "br_FR", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "bs_BA", fallback: "bs", yandex: "bs", name: "Bosnian", supported: true},
  {fb: "ca_ES", fallback: "ca", yandex: "ca", name: "Catalan", supported: true},
  {fb: "cb_IQ", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ck_US", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "co_FR", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "cs_CZ", fallback: "cs", yandex: "cs", name: "Czech", supported: true},
  {fb: "cx_PH", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "cy_GB", fallback: "cy", yandex: "cy", name: "Welsh", supported: true},
  {fb: "da_DK", fallback: "da", yandex: "da", name: "Danish", supported: true},
  {fb: "de_DE", fallback: "de", yandex: "de", name: "German", supported: true},
  {fb: "el_GR", fallback: "el", yandex: "el", name: "Greek", supported: true},
  {fb: "en_GB", fallback: "en", yandex: "en", name: "English", supported: true},
  {fb: "en_PI", fallback: "en", yandex: "en", name: "English", supported: true},
  {fb: "en_UD", fallback: "en", yandex: "en", name: "English", supported: true},
  {fb: "en_US", fallback: "en", yandex: "en", name: "English", supported: true},
  {fb: "eo_EO", fallback: "eo", yandex: "eo", name: "Esperanto", supported: true},
  {fb: "es_ES", fallback: "es", yandex: "es", name: "Spanish", supported: true},
  {fb: "es_LA", fallback: "es", yandex: "es", name: "Spanish", supported: true},
  {fb: "es_MX", fallback: "es", yandex: "es", name: "Spanish", supported: true},
  {fb: "et_EE", fallback: "et", yandex: "et", name: "Estonian", supported: true},
  {fb: "eu_ES", fallback: "eu", yandex: "eu", name: "Basque", supported: true},
  {fb: "fa_IR", fallback: "fa", yandex: "fa", name: "Persian", supported: true},
  {fb: "fb_LT", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ff_NG", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "fi_FI", fallback: "fi", yandex: "fi", name: "Finnish", supported: true},
  {fb: "fo_FO", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "fr_CA", fallback: "fr", yandex: "fr", name: "French", supported: true},
  {fb: "fr_FR", fallback: "fr", yandex: "fr", name: "French", supported: true},
  {fb: "fy_NL", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ga_IE", fallback: "ga", yandex: "ga", name: "Irish", supported: true},
  {fb: "gl_ES", fallback: "gl", yandex: "gl", name: "Galician", supported: true},
  {fb: "gn_PY", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "gu_IN", fallback: "gu", yandex: "gu", name: "Gujarati", supported: true},
  {fb: "gx_GR", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ha_NG", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "he_IL", fallback: "he", yandex: "he", name: "Hebrew", supported: true},
  {fb: "hi_IN", fallback: "hi", yandex: "hi", name: "Hindi", supported: true},
  {fb: "hr_HR", fallback: "hr", yandex: "hr", name: "Croatian", supported: true},
  {fb: "ht_HT", fallback: "ht", yandex: "ht", name: "Haitian", supported: true},
  {fb: "hu_HU", fallback: "hu", yandex: "hu", name: "Hungarian", supported: true},
  {fb: "hy_AM", fallback: "hy", yandex: "hy", name: "Armenian", supported: true},
  {fb: "id_ID", fallback: "id", yandex: "id", name: "Indonesian", supported: true},
  {fb: "ig_NG", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "is_IS", fallback: "is", yandex: "is", name: "Icelandic", supported: true},
  {fb: "it_IT", fallback: "it", yandex: "it", name: "Italian", supported: true},
  {fb: "ja_JP", fallback: "ja", yandex: "ja", name: "Japanese", supported: true},
  {fb: "ja_KS", fallback: "ja", yandex: "ja", name: "Japanese", supported: true},
  {fb: "jv_ID", fallback: "jv", yandex: "jv", name: "Javanese", supported: true},
  {fb: "ka_GE", fallback: "ka", yandex: "ka", name: "Georgian", supported: true},
  {fb: "kk_KZ", fallback: "kk", yandex: "kk", name: "Kazakh", supported: true},
  {fb: "km_KH", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "kn_IN", fallback: "kn", yandex: "kn", name: "Kannada", supported: true},
  {fb: "ko_KR", fallback: "ko", yandex: "ko", name: "Korean", supported: true},
  {fb: "ks_IN", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ku_TR", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ky_KG", fallback: "ky", yandex: "ky", name: "Kyrgyz", supported: true},
  {fb: "la_VA", fallback: "la", yandex: "la", name: "Latin", supported: true},
  {fb: "lg_UG", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "li_NL", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ln_CD", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "lo_LA", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "lt_LT", fallback: "lt", yandex: "lt", name: "Lithuanian", supported: true},
  {fb: "lv_LV", fallback: "lv", yandex: "lv", name: "Latvian", supported: true},
  {fb: "mg_MG", fallback: "mg", yandex: "mg", name: "Malagasy", supported: true},
  {fb: "mi_NZ", fallback: "mi", yandex: "mi", name: "Maori", supported: true},
  {fb: "mk_MK", fallback: "mk", yandex: "mk", name: "Macedonian", supported: true},
  {fb: "ml_IN", fallback: "ml", yandex: "ml", name: "Malayalam", supported: true},
  {fb: "mn_MN", fallback: "mn", yandex: "mn", name: "Mongolian", supported: true},
  {fb: "mr_IN", fallback: "mr", yandex: "mr", name: "Marathi", supported: true},
  {fb: "ms_MY", fallback: "ms", yandex: "ms", name: "Malay", supported: true},
  {fb: "mt_MT", fallback: "mt", yandex: "mt", name: "Maltese", supported: true},
  {fb: "my_MM", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "nb_NO", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "nd_ZW", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ne_NP", fallback: "ne", yandex: "ne", name: "Nepali", supported: true},
  {fb: "nl_BE", fallback: "nl", yandex: "nl", name: "Dutch", supported: true},
  {fb: "nl_NL", fallback: "nl", yandex: "nl", name: "Dutch", supported: true},
  {fb: "nn_NO", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "nr_ZA", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ns_ZA", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ny_MW", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "or_IN", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "pa_IN", fallback: "pa", yandex: "pa", name: "Punjabi", supported: true},
  {fb: "pl_PL", fallback: "pl", yandex: "pl", name: "Polish", supported: true},
  {fb: "ps_AF", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "pt_BR", fallback: "pt", yandex: "pt", name: "Portuguese", supported: true},
  {fb: "pt_PT", fallback: "pt", yandex: "pt", name: "Portuguese", supported: true},
  {fb: "qc_GT", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "qu_PE", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "qz_MM", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "rm_CH", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ro_RO", fallback: "ro", yandex: "ro", name: "Romanian", supported: true},
  {fb: "ru_RU", fallback: "ru", yandex: "ru", name: "Russian", supported: true},
  {fb: "rw_RW", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "sa_IN", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "sc_IT", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "se_NO", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "si_LK", fallback: "si", yandex: "si", name: "Sinhala", supported: true},
  {fb: "sk_SK", fallback: "sk", yandex: "sk", name: "Slovakian", supported: true},
  {fb: "sl_SI", fallback: "sl", yandex: "sl", name: "Slovenian", supported: true},
  {fb: "sn_ZW", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "so_SO", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "sq_AL", fallback: "sq", yandex: "sq", name: "Albanian", supported: true},
  {fb: "sr_RS", fallback: "sr", yandex: "sr", name: "Serbian", supported: true},
  {fb: "ss_SZ", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "st_ZA", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "sv_SE", fallback: "sv", yandex: "sv", name: "Swedish", supported: true},
  {fb: "sw_KE", fallback: "sw", yandex: "sw", name: "Swahili", supported: true},
  {fb: "sy_SY", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "sz_PL", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "ta_IN", fallback: "ta", yandex: "ta", name: "Tamil", supported: true},
  {fb: "te_IN", fallback: "te", yandex: "te", name: "Telugu", supported: true},
  {fb: "tg_TJ", fallback: "tg", yandex: "tg", name: "Tajik", supported: true},
  {fb: "th_TH", fallback: "th", yandex: "th", name: "Thai", supported: true},
  {fb: "tk_TM", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "tl_PH", fallback: "tl", yandex: "tl", name: "Tagalog", supported: true},
  {fb: "tl_ST", fallback: "tl", yandex: "tl", name: "Tagalog", supported: true},
  {fb: "tn_BW", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "tr_TR", fallback: "tr", yandex: "tr", name: "Turkish", supported: true},
  {fb: "ts_ZA", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "tt_RU", fallback: "tt", yandex: "tt", name: "Tatar", supported: true},
  {fb: "tz_MA", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "uk_UA", fallback: "uk", yandex: "uk", name: "Ukrainian", supported: true},
  {fb: "ur_PK", fallback: "ur", yandex: "ur", name: "Urdu", supported: true},
  {fb: "uz_UZ", fallback: "uz", yandex: "uz", name: "Uzbek", supported: true},
  {fb: "ve_ZA", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "vi_VN", fallback: "vi", yandex: "vi", name: "Vietnamese", supported: true},
  {fb: "wo_SN", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "xh_ZA", fallback: "xh", yandex: "xh", name: "Xhosa", supported: true},
  {fb: "yi_DE", fallback: "yi", yandex: "yi", name: "Yiddish", supported: true},
  {fb: "yo_NG", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "zh_CN", fallback: "zh", yandex: "zh", name: "Chinese", supported: true},
  {fb: "zh_HK", fallback: "zh", yandex: "zh", name: "Chinese", supported: true},
  {fb: "zh_TW", fallback: "zh", yandex: "zh", name: "Chinese", supported: true},
  {fb: "zu_ZA", fallback: "en", yandex: "en", name: "english", supported: false},
  {fb: "zz_TR", fallback: "en", yandex: "en", name: "english", supported: false}
]