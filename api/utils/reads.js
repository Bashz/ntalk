/**
 * ReadController
 *
 * @description :: Server-side logic for managing reads
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var sendAPI = require('../utils/sendAPI');

module.exports = function (user, fallback) {
  if (user.peer)
    sendAPI.read(user, fallback);
}

