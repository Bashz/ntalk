var messageController = require('./MessageController');
var postbackController = require('./postbackController');
var readController = require('./readController');

fallback = function (err, info) {
  if (err)
    return sails.log.error(err);
  sails.log.info(info);
};

module.exports = {
  route: function (msg, user) {
    if (msg.message)
      messageController(msg.message, user, fallback);
    else if (msg.postback)
      postbackController(msg.postback, user, fallback);
    else if (msg.read)
      readController(user, fallback);
  }
}