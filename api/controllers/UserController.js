/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  getFbUser: function (req, res) {
    User.getByFacebook(null, req.param("id"), function (err) {
      res.serverError({flash: {error: "Error while fetching user by fbid"}, data: err});
    }, function (user) {
      if (!user)
        res.notFound({flash: {error: "couldn't find this user"}, data: {}});
      else
        res.ok({flash: {log: "get"}, data: user});
    });
  },
  updateFbUser: function (req, res) {
    User.getByFacebook(null, req.param("id"), function (err) {
      res.serverError({flash: {error: "Error while fetching user by fbid"}, data: err});
    }, function (user) {
      if (!user)
        res.notFound({flash: {error: "couldn't find this user"}, data: {}});
      else {
        var data = req.allParams();
        user.intersted_by = data.interestedBy;
        user.country = data.country;
        user.tags = data.tags.split(',');
        user.save(function (err) {
          if (err)
            return res.serverError({flash: {error: "Error while savibg user"}, data: err});
          res.ok({flash: {log: "put"}, data: user});
        });
      }
    });
  }
};