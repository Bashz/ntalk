/**
 * BotController
 *
 * @description :: Server-side logic for managing bots
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var sendAPI = require('../utils/sendAPI');
var translate = require('yandex-translate')(sails.config.parameters.yandexKey);
var suid = require('rand-token').suid;
var languages = require('../utils/languages');

unreconizedPostback = function (postback) {
  sails.log.warn("Recived an unreconized postback, as bellow :");
  sails.log.info(postback);
};
fallback = function (err, info) {
  sails.log.info(new Date());
  if (err)
    return sails.log.error(err);
  sails.log.info(info);
};
reportError = function (user, err) {
  if (err)
    return sendAPI.reportError(user, err, fallback);
};

module.exports = {
  subscribe: function (req, res) {
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === sails.config.parameters.validationToken) {
      sails.log.info("Validating webhook");
      res.ok(req.query['hub.challenge']);
    } else {
      sails.log.error("Failed validation. Make sure the validation tokens match.");
      res.forbidden({err: "Failed validation. Make sure the validation tokens match."});
    }
  },
  handleMessage: function (req, res) {
    var data = req.allParams();
    data.entry.forEach(function (entry) {
      entry.messaging.forEach(function (message) {
        getUser(message.sender, function (err, user) {
          if (err)
            sails.log.error(err);
          if (message.message) {
            if (message.message.quick_reply) {
              var payload = message.message.quick_reply.payload;
              if (payload === "RECENT") {
                var page = parseInt(payload.split(':')[1]);
                return User.recents(user, reportError, function (users) {
                  sendAPI.contactItems(user, users, "You have not talked to anyone recently 😕", fallback);
                });
              } else if (payload === "FAVORIT") {
                var page = parseInt(payload.split(':')[1]);
                return User.favorites(user, reportError, function (users) {
                  sendAPI.contactItems(user, users, "You have not added a favorite contact yet 😕", fallback);
                });
              } else if (payload.match(/^SHOW_HUBS/)) {
                var page = parseInt(payload.split(':')[1]);
                return Hub.all(user, page, reportError, function (hubs) {
                  sendAPI.hubsTemplate(user, hubs, page, "SHOW_HUBS", fallback);
                });
              } else if (payload.match(/^MORE_FRIEND/)) {
                var page = parseInt(payload.split(':')[1]);
                return User.getOthers(user, page, reportError, function (users) {
                  sendAPI.contactTemplate(user, users, page, "MORE_FRIEND", fallback);
                });
              } else if (payload === "QUICK_CONNECT") {
                return User.queue(user, reportError, function (users) {
                  if (users.length) {
                    users.forEach(function (user) {
                      user.unbind(reportError, unbound, function () {
                      });
                    });
                    User.bind(users, reportError, function (users) {
                      sendAPI.bound(users, fallback);
                    });
                  } else {
                    sendAPI.text(user, "You are now in a queue we will match you soon with someone!", fallback);
                  }
                });
              } else if (payload === "NEARBY") {
                return sendAPI.askLocation(user, fallback);
              }
            }
            if (message.message.text) {
              var m = message.message.text.match(/^#([^\s]+)/);
              if (m) {
                return User.search(user, m[1], reportError, function (users, hubs) {
                  sendAPI.mixedItems(user, users, hubs, "No Result found, please try a more specific term\nexample: #ahmed or #bascketball", fallback);
                });
              }
            }
            if (message.message.attachments) {
              message.message.attachments.forEach(function (attachment) {
                if (attachment.type === 'location') {
                  user.setLatLong(attachment.payload.coordinates, reportError, function () {
                    User.nearBy(user, reportError, function (users) {
                      sendAPI.contactItems(user, users, "No user were found near you 😟", fallback);
                    });
                  });
                }
              });
            }
            if (user.creating_hub) {
              Hub.creation(user, message.message, reportError, function (nextQuestion) {
                sendAPI.text(user, nextQuestion, fallback);
              });
            } else if (user.peer) {
              if (message.message.text) {
                getLang(user.peer.locale, reportError, function (language) {
                  translate.translate(message.message.text, {to: language.yandex}, function (err, tr) {
                    if (tr.text[0] === message.message.text) {
                      getLang(user.locale, reportError, function (langFrom) {
                        translate.translate(message.message.text, {from: langFrom.yandex, to: language.yandex}, function (err, tr) {
                          sendAPI.translate(user, user.peer, tr, fallback);
                        });
                      });
                    } else {
                      sendAPI.translate(user, user.peer, tr, fallback);
                    }
                  });
                });
              } else if (message.message.attachments) {
                message.message.attachments.forEach(function (attachment) {
                  if (attachment.type !== 'location') {
                    sendAPI.sendAttachement(user, user.peer, attachment, fallback);
                  }
                });
              }
            } else if (user.room) {
              User.inUserHub(user, reportError, function (users) {
                if (!users)
                  return sendAPI.text(user, "You must join a hub first", fallback);
                users.forEach(function (hubUser) {
                  if (user.id !== hubUser.id) {
                    if (message.message.text) {
                      getLang(hubUser.locale, reportError, function (language) {
                        translate.translate(message.message.text, {to: language.yandex}, function (err, tr) {
                          if (tr.text[0] === message.message.text) {
                            getLang(user.locale, reportError, function (langFrom) {
                              translate.translate(message.message.text, {from: langFrom.yandex, to: language.yandex}, function (err, tr) {
                                sendAPI.translate(user, hubUser, tr, fallback);
                              });
                            });
                          } else {
                            sendAPI.translate(user, hubUser, tr, fallback);
                          }
                        });
                      });
                    } else if (message.message.attachments) {
                      message.message.attachments.forEach(function (attachment) {
                        if (attachment.type !== 'location') {
                          sendAPI.sendAttachement(user, hubUser, attachment, fallback);
                        }
                      });
                    }
                  }
                });
              });
            } else {
              if (message.message.attachments) {
                if (!message.message.attachments.find(function (attachment) {
                  return attachment.type === 'location';
                }))
                  return sendAPI.noPeer(user, fallback);
              } else {
                return sendAPI.noPeer(user, fallback);
              }
            }
          }

          if (message.postback) {
            var payload = message.postback.payload;
            if (payload === "MENU_DISCONNECT") {
              user.unbind(reportError, unbound, function () {
                user.disconnect(reportError, function () {
                  sendAPI.text(user, "You are now disconnected\nYou can not be requested to chat and will not receive messages", fallback);
                });
              });
            } else if (payload === "MENU_CONNECT") {
              user.connect(reportError, function () {
                sendAPI.connected(user, fallback);
              });
            } else if (payload.match(/^MORE_FRIEND/)) {
              var page = parseInt(payload.split(':')[1]);
              User.getOthers(user, page, reportError, function (users) {
                sendAPI.contactTemplate(user, users, page, "MORE_FRIEND", fallback);
              });
            } else if (payload === "QUICK_CONNECT") {
              User.queue(user, reportError, function (users) {
                if (users.length) {
                  users.forEach(function (user) {
                    user.unbind(reportError, unbound, function () {
                    });
                  });
                  User.bind(users, reportError, function (users) {
                    sendAPI.bound(users, fallback);
                  });
                } else {
                  sendAPI.text(user, "You are now in a queue we will match you soon with someone!", fallback);
                }
              });
            } else if (payload.match(/^SHOW_HUB_USERS/)) {
              var page = parseInt(payload.split(':')[1]);
              User.inUserHub(user, reportError, function (users) {
                if (!users)
                  return sendAPI.text(user, "You must join a hub first", fallback);
                sendAPI.contactTemplate(user, users, page, "SHOW_HUB_USERS", fallback);
              });
            } else if (payload.match(/^SHOW_HUBS/)) {
              var page = parseInt(payload.split(':')[1]);
              Hub.all(user, page, reportError, function (hubs) {
                sendAPI.hubsTemplate(user, hubs, page, "SHOW_HUBS", fallback);
              });
            } else if (payload === "CREATE_HUB") {
              user.creatingHub(reportError, function () {
                sendAPI.text(user, "👍 Give your new hub a name", fallback);
              });
            } else if (payload === "DISCONNECT_HUB") {
              user.leaveHub(reportError, function (hub) {
                if (hub)
                  sendAPI.text(user, "Leaving: " + hub.name, function (err, msg) {
                    user.unbind(reportError, unbound, function () {
                    });
                  });
                else
                  sendAPI.text(user, "You do not belong to any hub :p", fallback);
              });

            } else if (payload.match(/CONNECT_HUB/)) {
              var hubId = payload.split(':')[1];
              if (user.room && hubId === user.room.id)
                return sendAPI.text(user, "You are already in this hub :p", fallback);
              user.unbind(reportError, unbound, function () {
                user.joinHub(hubId, reportError, function (hub) {
                  sendAPI.text(user, "Successfully joined " + hub.name + "\n" + (hub.welcome_msg ? hub.welcome_msg : ""), fallback);
                  User.findByHub(user, hub, reportError, function (users) {
                    users.forEach(function (inhub) {
                      sendAPI.hubConnected(inhub, user, fallback);
                    });
                  });
                });
              });
            } else if (payload === "RECENT") {
              var page = parseInt(payload.split(':')[1]);
              User.recents(user, reportError, function (users) {
                sendAPI.contactItems(user, users, "You have not talked to anyone recently 😕", fallback);
              });
            } else if (payload === "FAVORIT") {
              var page = parseInt(payload.split(':')[1]);
              User.favorites(user, reportError, function (users) {
                sendAPI.contactItems(user, users, "You have not added a favorite contact yet 😕", fallback);
              });
            } else if (payload.match(/^ADD_FAVORIT/)) {
              var userId = payload.split(':')[1];
              User.addFavorit(user, userId, reportError, function (favorit) {
                sendAPI.text(user, "⭐" + favorit.first_name + " " + favorit.last_name + " successfuly added to your favorits !!", fallback);
              });
            } else if (payload === "SEARCH") {
              return sendAPI.text(user, "Type # followed by your keywrods to search among users and hubs\nexample #mohamed , #tunisia or #soccer", fallback);
            } else if (payload.match(/MENU_LANG/)) {
              var lang = payload.split(':')[1];
              getLang(lang, reportError, function (language) {
                user.setLang(language, reportError, function () {
                  sendAPI.text(user, "Language changed to " + language.name, fallback);
                });
              });
            } else if (payload.match(/^REQUEST_TO/)) {
              var userFbId = payload.split(':')[1];
              User.getByFb(user, userFbId, reportError, function (requested) {
                sendAPI.requestingTalk(user, requested, fallback);
                sendAPI.requestedTalk(requested, user, fallback);
              })
            } else if (payload.match(/^CONNECT_TO/)) {
              var userFbId = payload.split(':')[1];
              user.unbind(reportError, unbound, function () {
                User.getByFb(user, userFbId, reportError, function (peer) {
                  User.bind([user, peer], reportError, function (users) {
                    sendAPI.bound(users, fallback);
                  });
                });
              });
            } else if (payload === "NEARBY") {
              return sendAPI.askLocation(user, fallback);
            } else if (payload === "CONTACTS") {
              return sendAPI.qrContacts(user, fallback);
            } else if (payload === "PROFILE") {
              return sendAPI.myProfile(user, fallback);
            } else {
              unreconizedPostback(message.postback);
            }
          } else {
            //unreconizedPostback(message);
          }

          if (message.delivery)
            return;

          if (message.read)
            if (user.peer)
              sendAPI.read(user, fallback);
        });
      });
    });
    res.ok();
  },
  authorize: function (req, res) {
    var accountLinkingToken = req.query.account_linking_token;
    var redirectURI = req.query.redirect_uri;
    var authCode = "5416074100";
    var redirectURISuccess = redirectURI + "&authorization_code=" + authCode;

    res.render('authorize', {
      accountLinkingToken: accountLinkingToken,
      redirectURI: redirectURI,
      redirectURISuccess: redirectURISuccess
    });
  }
};

getUser = function (sender, cb) {
  if (!sender)
    cb('can not find sender', null);
  User.findOne({fbId: sender.id})
      .populate('peer')
      .populate('room')
      .exec(function (err, user) {
        if (err)
          cb(err, null);
        if (!user) {
          User.createFromFb(sender.id, function (err, user) {
            if (!err)
              sendAPI.welcome(user, function (err, message) {
                if (err)
                  return sails.log.error(err);
                sails.log.info(message);
              });
          });
        } else {
          cb(null, user);
        }
      });
};
unbound = function (peer, hub, user) {
  if (peer) {
    sendAPI.peerDisconnected(peer, user, fallback);
    sendAPI.text(user, "You are no longer talking to " + peer.first_name + " " + peer.last_name, fallback);
  }
  if (hub) {
    sendAPI.text(user, "You left the hub:  " + hub.name, fallback);
    User.findByHub(user, hub, reportError, function (users) {
      users.forEach(function (inhub) {
        sendAPI.hubDisconnected(inhub, user, fallback);
      });
    });
  }
}
getLang = function (fblang, error, success) {
  var fb = languages.map(function (lang) {
    return lang.fb;
  });
  var ind = fb.indexOf(fblang);
  if (ind !== -1) {
    success(languages[ind]);
  } else {
    error("Language was not found");
  }
}